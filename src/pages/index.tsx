import React from 'react'
import { useAppContext } from '@context/app.context'
import Button from '@components/Button'
// import useApi from '@hooks/api.hook'
// import useProducts from '@hooks/products.hook'
// import { Products } from '@interfaces/products.namespace'

export default function IndexPage(): JSX.Element {
  // const { get } = useApi()
  // const { getProductById } = useProducts()
  const context = useAppContext()

  // const [product, setProduct] =
  //   React.useState<Products.Product | undefined>(undefined)

  React.useEffect(() => {
    // (async () => {
    //   await get()
    //   setProduct(await getProductById(124))
    // })()
  }, [])

  return (
    <>
      <h1>
        Bienvenido a LuegopaGo SOAT{' '}
        <span role="img" aria-label="Hello">
          👋
        </span>
      </h1>
      {/* <p>{ product.dimensions.height }</p> */}
      <Button label="Hello!" message={context.contextMethod()} />
    </>
  )
}
