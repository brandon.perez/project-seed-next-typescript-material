import Button from '@components/Button'
import { useAppContext } from '@context/app.context'

export default function Post(): JSX.Element {
  const context = useAppContext()
  return (
    <>
      <h1>Página Posts</h1>
      <p>{context.contextMethod()}</p>
      <Button
        label="Ver post"
        message="Mira -> src/constants/app.constants.ts:1 ;)"
      />
    </>
  )
}
