import Head from 'next/head'
import { AppProps } from 'next/app'
import CssBaseline from '@material-ui/core/CssBaseline'
import LayOut from '@layout'

import AppContextProvider from '@context/app.context'

export default function App(props: AppProps): JSX.Element {
  const { Component, pageProps } = props
  return (
    <div>
      <Head>
        <title>LuegopaGo - SOAT</title>
        <link
          rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
        />
        <link
          rel="stylesheet"
          href="https://fonts.googleapis.com/icon?family=Material+Icons"
        />
      </Head>
      <AppContextProvider>
        <CssBaseline />
        <LayOut>
          <Component {...pageProps} />
        </LayOut>
      </AppContextProvider>
    </div>
  )
}
