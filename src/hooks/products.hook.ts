import Request from '@utils/request.axios'
import { BASE_URL } from '@constants/api.constants'
import { Products } from '@interfaces/products.namespace'

interface IUserProducts {
  getProducts: () => Promise<Products.Product[]>
  getProductById: (id: string) => Promise<Products.Product>
  createProduct: (data: Products.ProductDto) => Promise<Products.Product>
  updateProduct: (
    id: string,
    data: Products.Product
  ) => Promise<Products.Product>
  deleteProduct: (id: string) => void
}

export default function useProducts(): IUserProducts {
  const getProducts = async (): Promise<Products.Product[]> =>
    await Request({ url: BASE_URL, method: 'GET' })

  const getProductById = async (id: string): Promise<Products.Product> =>
    await Request({ url: `${BASE_URL}/${id}`, method: 'GET' })

  const createProduct = async (
    data: Products.ProductDto
  ): Promise<Products.Product> =>
    await Request({ url: BASE_URL, method: 'POST', data })

  const updateProduct = async (
    id: string,
    data: Products.ProductDto
  ): Promise<Products.Product> =>
    await Request({ url: `${BASE_URL}/${id}`, method: 'PUT', data })

  const deleteProduct = async (id: string): Promise<void> =>
    await Request({ url: `${BASE_URL}/${id}`, method: 'DELETE' })

  return {
    getProducts,
    getProductById,
    createProduct,
    updateProduct,
    deleteProduct,
  }
}
