import Request from '@utils/request.axios'
import { BASE_URL } from '@constants/api.constants'
import { Api } from '@interfaces/api.namespace'

interface IUseApi {
  get: () => Promise<Api.Object[]>
  getById: (id: string) => Promise<Api.Object>
  post: (data: Api.CreateObjectDto) => Promise<Api.Object>
  update: (id: string) => Promise<Api.Object>
}

export default function useApi(): IUseApi {
  const get = async (): Promise<Api.Object[]> =>
    await Request<Api.Object[]>({ url: BASE_URL, method: 'GET' })

  const getById = async (id: string): Promise<Api.ObjectTwo> =>
    await Request<Api.ObjectTwo>({ url: `${BASE_URL}/${id}`, method: 'GET' })

  const post = async (data: Api.CreateObjectDto): Promise<Api.Object> =>
    await Request<Api.Object>({ url: BASE_URL, method: 'POST', data: data })

  const update = async (id: string): Promise<Api.Object> =>
    await Request<Api.Object>({ url: BASE_URL, method: 'PUT', data: id })

  return {
    get,
    post,
    update,
    getById,
  }
}
