import Axios, {
  AxiosInstance,
  AxiosRequestConfig,
  AxiosResponse,
  AxiosError,
} from 'axios'

const axiosInstance: AxiosInstance = Axios.create()

axiosInstance.interceptors.request.use(
  (config: AxiosRequestConfig): AxiosRequestConfig => {
    return config
  },
  (error): Promise<AxiosError> => {
    return Promise.reject(error)
  }
)

axiosInstance.interceptors.response.use(
  (response: AxiosResponse): AxiosResponse => {
    return response
  },
  (error): Promise<AxiosError> => {
    return Promise.reject(error)
  }
)

export default axiosInstance
