import { AxiosRequestConfig } from 'axios'
import Axios from '@utils/axios.interceptor'

export default async function request<T>(
  config: AxiosRequestConfig
): Promise<T> {
  const response = await Axios(config)
  return response.data
}
