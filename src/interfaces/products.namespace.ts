export namespace Products {
  export interface ProductDto {
    id: number
    name: string
    price: string
    regular_price: string
    sale_price: string
  }

  export interface Product {
    id: number
    name: string
    price: string
    regular_price: string
    sale_price: string
    attributes: Attribute[]
    dimensions: Dimension
    categories: Category[]
    variations: Variation[]
  }

  export interface Attribute {
    id: number
    name: string
    options: string[]
  }

  export interface Dimension {
    height: string
    width: string
    length: string
  }

  export interface Category {
    id: number
    name: string
    slug: string
  }

  export interface Variation {
    id: number
    name: string
    price: string
    regular_price: string
    sale_price: string
    dimensions: Dimension[]
  }
}
