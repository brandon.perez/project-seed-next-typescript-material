export namespace Api {
  export interface CreateObjectDto {
    id: string
    name: string
  }

  export interface Object {
    id: string
    name: string
  }

  export interface ObjectTwo extends Object {
    address: string
    phone: string
  }
}

export {}
