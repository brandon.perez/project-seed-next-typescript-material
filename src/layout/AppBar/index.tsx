/* eslint-disable jsx-a11y/anchor-is-valid */
import Link from 'next/link'

export default function AppBar(): JSX.Element {
  return (
    <header>
      <nav>
        <Link href="/">
          <a>Inicio</a>
        </Link>
        {' | '}
        <Link href="/posts">
          <a>Posts</a>
        </Link>
      </nav>
    </header>
  )
}
