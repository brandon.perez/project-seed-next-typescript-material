import { ReactNode } from 'react'
import AppBar from './AppBar'

interface Props {
  children?: ReactNode
}

export default function LayOut({ children }: Props): JSX.Element {
  return (
    <div>
      <AppBar />
      {children}
    </div>
  )
}
