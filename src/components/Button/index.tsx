import Button from '@material-ui/core/Button'

interface Props {
  label: string
  message: string
}

export default function CustomButton({ label, message }: Props): JSX.Element {
  return (
    <Button onClick={() => alert(message)} variant="contained" color="primary">
      {label}
    </Button>
  )
}
