import React from 'react'

interface Props {
  children?: React.ReactNode
}

interface IAppContext {
  contextMethod: () => string
}

const AppContext = React.createContext<IAppContext | undefined>(undefined)

export default function AppContextProvider({ children }: Props): JSX.Element {
  const contextMethod = () => 'Esto viene del context! :D'

  return (
    <AppContext.Provider value={{ contextMethod }}>
      {children}
    </AppContext.Provider>
  )
}

export const useAppContext = (): IAppContext => React.useContext(AppContext)
